# idtrackerai-app

⚠️ This package is deprecated! ⚠️

It was part of [idtracker.ai](https://gitlab.com/polavieja_lab/idtrackerai/) but since version 5.0 it has been unused. Check the [idtracker.ai webpage](https://idtracker.ai/) for more information about the main software.

