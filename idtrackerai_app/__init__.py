__version__ = "0.0.13"

import locale

locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
locale.setlocale(locale.LC_NUMERIC, "en_US.UTF-8")
